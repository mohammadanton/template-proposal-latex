# Template Proposal LaTeX

## Panduan

Silahkan merujuk pada [docs](docs/)

## Mengapa LaTeX ?

Sudah banyak sekali sumber yang dapat menjawab pertanyaan ini secara
detail. Cukup lakukan pencarian dengan kata kunci "why latex". Tetapi
'opinionated reason' saya adalah:

- Saya mengerjakan banyak hal di
  [Emacs](https://www.gnu.org/software/emacs/). [org-mode](http://orgmode.org/)
  cukup baik untuk membuat banyak dokumen yang saya perlukan. Tetapi
  saya butuh [LaTeX](https://www.latex-project.org/) untuk membuat
  dokumen yang lebih kompleks.
- Saya dapat dengan mudah menggunakan *revision control*. Sehingga
  tidak ada lagi dokumen dengan nama 'jadi1', 'jadibanget',
  'jadifinal'.
- Kemudahan sitasi. tidak perlu lagi menulis semua sitasi secara manual.
- Kemudahan untuk membuat *branch* baru ketika ada revisi. Sehingga
  saya mudah untuk mengatur dokumen dengan perubahan mana yang
  harus saya gunakan.
- Kelebihan semua *revision control* tentunya.
- Menulis LaTeX di Emacs dengan bantuan beberapa packages. Saya
  dapat dengan mudah mendapatkan fitur *completion* pada sitasi dan
  beragam fitur lainnya.
- Saya dapat fokus pada konten dan tidak terdistraksi dengan gambar
  disana sini.
- Saya cukup terbiasa menggunakan *keybinding* pada Emacs. Tentu butuh
  waktu yang lebih lama jika bekerja diluar.
- *I get homesick when I'm outside Emacs* &#x1f60e;

*PS: tentu anda dapat menggunakan text-editor kesukaan anda*

*Silahkan tambahkan alasan anda pada branch kontribusi yang anda buat* :)


## Hasil Ekspor

Hasil ekspor ke
[pdf](/uploads/b9fce4ce2e77fd8f8ade938f5adc32a3/proposal.pdf)

*PS: ekspor ini bisa jadi tidak sesuai dengan perubahan terbaru*

## Daftar Template

- Filkom-UB


## Pengembang

Lihat [AUTHORS](https://github.com/azzamsa/template-skripsi-id/blob/master/AUTHORS)

